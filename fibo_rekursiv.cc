//Uebung 03 Aufgabe 2b) Schreiben Sie ein Programm, dass ebenfalls von der Kommandozeile einliest, jedoch auf rekursive Weise die Folge berechnet.

#include <iostream>

int fiboRekursiv(int n)
{
    if (n<1)
        return -1;
    else if (n == 1)
        return 0;
    else if (n == 2)
        return 1;
    else
        return fiboRekursiv(n-1) + fiboRekursiv(n-2);
}



int main(){
    
    int n;
    std::cout << "Please input number of elements, n= " << std::endl;
    std::cin >> n;
    
    int i = 0;
    while(++i < n)
        std::cout << fiboRekursiv(i) << ",";
    std::cout << fiboRekursiv(n) << std::endl;
    
    return 0;
    
}
