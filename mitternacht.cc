//Uebung 03 Aufgabe 1 Schreiben Sie ein Programm mit zwei Funktionen, das von der Kommandozeile die Koeffizienten einer quadratischen Gleichung der Form ax2+bx+c = 0 abfragt und mit der Mitternachtsformel die beiden Nullstellen ausrechnet und ausgibt. 

#include <iostream>
#include <cmath>


void showZeros(double a, double b, double c)
{ 
    if(a==0 && b==0)
        std::cout << "Fehler" << std::endl; //Error: No solution exists due to invalid coefficients
    else if(b*b < 4*a*c)
        std::cout << "Komplexe Zahl" << std::endl; //Error: No solution exists due to complex roots
    else {
        double rt = std::sqrt(b*b - 4*a*c);
        if (rt == 0){
            double x = -b/(2*a);
            std::cout << "x = " << x << std::endl;
        }
        else {
        double x1 = (-b + rt)/(2*a);
        double x2 = (-b - rt)/(2*a);
        std::cout << "x1=" << x1 << ",x2=" << x2 << std::endl;
        }
    }
}

int main(){
    double a; 
    double b; 
    double c;

    std::cout << "a = " << std::flush;
    std::cin >> a;

    std::cout << "b = " << std::flush;
    std::cin >> b;
    
    std::cout << "c = " << std::flush;
    std::cin >> c;
 
    showZeros(a,b,c);

    return 0;
}
