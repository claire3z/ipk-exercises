//Uebung 03 Aufgabe 2a) schreiben Sie ein Programm, das fN ausgibt, wobei N von der Kommandozeile eingelesen werden sollten. Verwenden Sie als Datentyp für die Zahlen int.

#include <iostream>

int fiboIter(int numberOfElements)
{
    int N;
    if (numberOfElements < 1)
        N = -1;
    else if (numberOfElements == 1)
        N = 0;
    else if (numberOfElements == 2)
        N = 1;
    else {
        int N_1 = 1;
        int N_2 = 0;
        int i;
        for (i=3;i<=numberOfElements;i++){
            N = N_1 + N_2;
            N_2 = N_1;
            N_1 = N;
        }
    }
    return N;
}


int main(){ 
    int n;
    std::cout << "Please input number of elements, n= " << std::endl;
    std::cin >> n;
    
    int i = 0;
    while(++i < n)
        std::cout << fiboIter(i) << ",";
    std::cout << fiboIter(n) << std::endl;
    return 0;
}
